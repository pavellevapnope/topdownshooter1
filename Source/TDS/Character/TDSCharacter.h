// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "TDS/FuncLibrary/Types.h"
#include "TDSCharacter.generated.h"

class UInputComponent;

UCLASS(Blueprintable)
class ATDSCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	ATDSCharacter();


	virtual void SetupPlayerInputComponent(UInputComponent* inputComponent) override;

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	/** Returns TopDownCameraComponent subobject **/
	FORCEINLINE class UCameraComponent* GetTopDownCameraComponent() const { return TopDownCameraComponent; }
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

private:
	/** Top down camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* TopDownCameraComponent;

	/** Camera boom positioning the camera above the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		EMovementState MovementState = EMovementState::State_Walk;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		FStateSpeed StateSpeed;
	
	float ResSpeed = 200.0f;

	UPROPERTY()
		bool isForwardAxis = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bRun = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bAim = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bWalk = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		bool bWalkFast = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character's state")
		bool bWeaponIsEquimpent = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character's state")
		float maxStamina = 12000;
		float minStamina = -100.0f;
		float Stamina = maxStamina;
		float StaminaDownMark = 300.0f;
		float DecreaseStaminaCoeficient = 0.003;
		float IcreaseStaminaCoeficient = 1.25;
		float InterpolationSpeed = 0.8;
	UFUNCTION()
		void InputAxisX(float Value);
	UFUNCTION()
		void InputAxisY(float Value);
	UPROPERTY(BlueprintReadWrite)
		float AxisX = 0.0f;
	UPROPERTY(BlueprintReadWrite)
		float AxisY = 0.0f;

	UFUNCTION()
		void MovementTick(float DeltaTime);

	UFUNCTION(BlueprintCallable)
		void CharacterUpdate();

	UFUNCTION(BlueprintCallable)
		void CharacterMovementState();

	UFUNCTION(BlueprintCallable)
		void StaminaStateUpdate();

	UFUNCTION(BlueprintCallable)
		void MoveFunction(bool ForwardAxis);
};

