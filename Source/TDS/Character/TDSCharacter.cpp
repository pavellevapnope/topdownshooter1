// Copyright Epic Games, Inc. All Rights Reserved.

#include "TDSCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "Camera/CameraComponent.h"
#include "Components/DecalComponent.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/PlayerController.h"
#include "GameFramework/SpringArmComponent.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Materials/Material.h"
#include "Math/UnrealMathUtility.h"
#include "Kismet/GameplayStatics.h"
#include "Kismet/KismetMathLibrary.h"
#include "Engine/World.h"

ATDSCharacter::ATDSCharacter()
{
	// Set size for player capsule
	GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);

	// Don't rotate character to camera direction
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = true; // Rotate character to moving direction
	GetCharacterMovement()->RotationRate = FRotator(0.f, 640.f, 0.f);
	GetCharacterMovement()->bConstrainToPlane = true;
	GetCharacterMovement()->bSnapToPlaneAtStart = true;

	// Create a camera boom...
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
	CameraBoom->TargetArmLength = 800.f;
	CameraBoom->SetRelativeRotation(FRotator(-60.f, 0.f, 0.f));
	CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

	// Create a camera...
	TopDownCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("TopDownCamera"));
	TopDownCameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
	TopDownCameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("/Game/Blueprint/Character/M_Cursor_Decal.M_Cursor_Decal"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	PrimaryActorTick.bStartWithTickEnabled = true;

	GetCharacterMovement()->MaxWalkSpeed = 200.f;
}


void ATDSCharacter::SetupPlayerInputComponent(UInputComponent* inputComponent)
{

	Super::SetupPlayerInputComponent(inputComponent);

	inputComponent->BindAxis(TEXT("MoveForward"), this, &ATDSCharacter::InputAxisX);
	inputComponent->BindAxis(TEXT("MoveRight"), this, &ATDSCharacter::InputAxisY);
}

void ATDSCharacter::Tick(float DeltaSeconds)
{
    Super::Tick(DeltaSeconds);

	/*if (CursorToWorld != nullptr)
	{
		if (UHeadMountedDisplayFunctionLibrary::IsHeadMountedDisplayEnabled())
		{
			if (UWorld* World = GetWorld())
			{
				FHitResult HitResult;
				FCollisionQueryParams Params(NAME_None, FCollisionQueryParams::GetUnknownStatId());
				FVector StartLocation = TopDownCameraComponent->GetComponentLocation();
				FVector EndLocation = TopDownCameraComponent->GetComponentRotation().Vector() * 2000.0f;
				Params.AddIgnoredActor(this);
				World->LineTraceSingleByChannel(HitResult, StartLocation, EndLocation, ECC_Visibility, Params);
				FQuat SurfaceRotation = HitResult.ImpactNormal.ToOrientationRotator().Quaternion();
				CursorToWorld->SetWorldLocationAndRotation(HitResult.Location, SurfaceRotation);
			}
		}
		else if (APlayerController* PC = Cast<APlayerController>(GetController()))
		{
			FHitResult TraceHitResult;
			PC->GetHitResultUnderCursor(ECC_Visibility, true, TraceHitResult);
			FVector CursorFV = TraceHitResult.ImpactNormal;
			FRotator CursorR = CursorFV.Rotation();
			CursorToWorld->SetWorldLocation(TraceHitResult.Location);
			CursorToWorld->SetWorldRotation(CursorR);
		}
	}*/
	MovementTick(DeltaSeconds);

	StaminaStateUpdate();


}

void ATDSCharacter::InputAxisX(float Value)
{
	AxisX = Value;
	isForwardAxis = true;
	MoveFunction(isForwardAxis);
}

void ATDSCharacter::InputAxisY(float Value)
{
	AxisY = Value;
	isForwardAxis = false;
	MoveFunction(isForwardAxis);
}

void ATDSCharacter::MovementTick(float DeltaTime)
{

	/*APlayerController* myController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
	if (myController)
	{
		FHitResult HitResult;
		myController->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery6, false, HitResult);
		float FindRotatorResultYaw = UKismetMathLibrary::FindLookAtRotation(GetActorLocation(), HitResult.Location).Yaw;
		if(ResSpeed == StateSpeed.SpeedRun)
		{
			SetActorRotation(FQuat(FRotator(0.0f, GetActorRotation().Yaw, 0.0f)));
		}
		else
		{
			SetActorRotation(FQuat(FRotator(0.0f, FindRotatorResultYaw, 0.0f)));
		}
	}*/

	//FVector ForwardVector = TopDownCameraComponent->GetForwardVector();
	//FVector RightVector = TopDownCameraComponent->GetRightVector();
}

void ATDSCharacter::MoveFunction(bool ForwardAxis)
{
 	FRotator Rot = Controller->GetControlRotation();
 	FRotator YawRot(0, Rot.Yaw, 0);
	if (ForwardAxis)
		{FVector DirectionForward = FRotationMatrix(YawRot).GetUnitAxis(EAxis::X);
		AddMovementInput(DirectionForward, AxisX);} 
	else 
		{FVector DirectionRight = FRotationMatrix(YawRot).GetUnitAxis(EAxis::Y);	
		AddMovementInput(DirectionRight, AxisY);}


	if(bAim && ResSpeed <= 200)
	 	{SetActorRotation(YawRot);}
}

void ATDSCharacter::CharacterUpdate()
{
	
	switch (MovementState)
	{
	case EMovementState::State_Walk:
		ResSpeed = StateSpeed.SpeedWalk;
		break;
	case EMovementState::State_Aim:
		ResSpeed = StateSpeed.SpeedAim;
		break;
	case EMovementState::State_Run:
		ResSpeed = StateSpeed.SpeedRun;
		break;
	case EMovementState::State_WalkFast:
		ResSpeed = StateSpeed.SpeedWalkFast;
		break;
	default:
		break;
	}

	GetCharacterMovement()->MaxWalkSpeed = ResSpeed;
	StaminaStateUpdate();
}


void ATDSCharacter::CharacterMovementState()
{
	if(bRun || bAim || bWalkFast)
	{	
		if(bRun && Stamina >= 0 && !bAim)
		{
			MovementState = EMovementState::State_Run;
		}
		else if(bAim && bRun)
		{
			MovementState = EMovementState::State_Walk;
		}
		else if(bAim)
		{
			MovementState = EMovementState::State_Aim;
		}
		else if(bWalkFast && Stamina >= 0)
		{
			MovementState = EMovementState::State_WalkFast;
		}
	}
	else
	{
		MovementState = EMovementState::State_Walk;
	}
	
	CharacterUpdate();
}

void ATDSCharacter::StaminaStateUpdate()
{

	if(ResSpeed > StaminaDownMark)
	{
		Stamina -= ResSpeed * DecreaseStaminaCoeficient;
	}
	else if(Stamina < maxStamina)
	{
		Stamina += IcreaseStaminaCoeficient;
	}

	if(Stamina < minStamina)
	{
		GetCharacterMovement()->MaxWalkSpeed = 200.f;
	}

}
