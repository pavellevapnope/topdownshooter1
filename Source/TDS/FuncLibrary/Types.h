// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "Types.generated.h"

/**
 * 
 */

UENUM(BlueprintType)
enum class EMovementState : uint8
{
	State_Walk UMETA(DisplayName = "Walk"),
	State_Aim UMETA(DisplayName = "Aim"),
	State_Run UMETA(DisplayName = "Run"),
	State_WalkFast UMETA(DisplayName = "WalkFast")
};

USTRUCT(BlueprintType)
struct FStateSpeed
{


	GENERATED_BODY()


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float SpeedWalk = 200.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float SpeedRun = 600.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float SpeedAim = 120.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float SpeedWalkFast = 350.0f;
};

UCLASS()
class TDS_API UTypes : public UBlueprintFunctionLibrary
{

	GENERATED_BODY()


};
